#include <error.h>

#include "json_helper.hpp"

void hasMember(rapidjson::Document &json_document, std::string member)
{
	if (!json_document.HasMember(member.c_str()))
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "Missing member: %s", member.c_str());
	}
}

const rapidjson::Value &getArray(rapidjson::Document &json_document, std::string member, rapidjson::SizeType size)
{
	hasMember(json_document, member);
	if (!json_document[member.c_str()].IsArray())
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "Member \"%s\" should be of type %s", member.c_str(), "array");
	}
	if (json_document[member.c_str()].Size() != size)
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "Member \"%s\" should be of size %u", member.c_str(), size);
	}
	return json_document[member.c_str()];
}

double getDouble(rapidjson::Document &json_document, std::string member)
{
	hasMember(json_document, member);
	if (!json_document[member.c_str()].IsDouble())
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "Member \"%s\" should be of type %s", member.c_str(), "double");
	}
	return json_document[member.c_str()].GetDouble();
}

int getInt(rapidjson::Document &json_document, std::string member)
{
	hasMember(json_document, member);
	if (!json_document[member.c_str()].IsInt())
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "Member \"%s\" should be of type %s", member.c_str(), "int");
	}
	return json_document[member.c_str()].GetInt();
}

unsigned int getUint(rapidjson::Document &json_document, std::string member)
{
	hasMember(json_document, member);
	if (!json_document[member.c_str()].IsUint())
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "Member \"%s\" should be of type %s", member.c_str(), "unsigned int");
	}
	return json_document[member.c_str()].GetUint();
}
