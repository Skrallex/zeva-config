#pragma once

#include <rapidjson/document.h>

const rapidjson::Value &getArray(rapidjson::Document &json_document, std::string member, rapidjson::SizeType size);
double getDouble(rapidjson::Document &json_document, std::string member);
int getInt(rapidjson::Document &json_document, std::string member);
unsigned int getUint(rapidjson::Document &json_document, std::string member);
