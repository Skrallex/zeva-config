#pragma once

#include <string>

class CAN
{
public:
	CAN(std::string port);
	~CAN();

	void write(uint32_t can_id, uint8_t data_length, uint8_t *data);

private:
	int socket_fd;
	const std::string port;

	void openPort();
	void closePort();
};
