#include <error.h>
#include <rapidjson/document.h>
#include <rapidjson/error/en.h>
#include <rapidjson/filereadstream.h>

#include "build_info.hpp"
#include "CAN.hpp"
#include "json_helper.hpp"

int main(int argc, char **argv)
{

	/* Parse input arguments */

	if (argc != 3)
	{
		printf("Version: %s (%s)\n", build_info::version, build_info::branch);
		error_at_line(-1, 0, __FILE__, __LINE__, "Usage: %s CAN_DEVICE CONFIG_FILE", program_invocation_short_name);
	}
	char *can_device = argv[1];
	char *json_file_name = argv[2];

	/* Parse JSON config */

	// Open file
	FILE *json_file = fopen(json_file_name, "r");
	if (!json_file)
	{
		error_at_line(errno, errno, __FILE__, __LINE__, "%s", json_file_name);
	}

	// Parse file
	char json_buffer[65536];
	rapidjson::FileReadStream json_stream(json_file, json_buffer, sizeof(json_buffer));
	rapidjson::Document json_document;
	json_document.ParseStream(json_stream);

	// Close file
	if (fclose(json_file))
	{
		error_at_line(errno, errno, __FILE__, __LINE__, "fclose");
	}

	// Check config for errors
	if (json_document.HasParseError())
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "JSON parse error at character %zu: %s", json_document.GetErrorOffset(), rapidjson::GetParseError_En(json_document.GetParseError()));
	}

	/* Convert JSON config to CAN frames */

	// Config 1
	uint8_t config_1[8];

	config_1[0] = getUint(json_document, "Pack Capacity") / 5;
	config_1[1] = getUint(json_document, "SoC Warning");
	config_1[2] = getUint(json_document, "Full Voltage") / 2;
	config_1[3] = getUint(json_document, "Warn Current") / 10;
	config_1[4] = getUint(json_document, "Trip Current") / 10;
	config_1[5] = getUint(json_document, "EVMS Temp Warning");
	config_1[6] = getUint(json_document, "Min Aux Voltage");
	config_1[7] = getUint(json_document, "Min Isolation");

	// Config 2
	uint8_t config_2[8];

	config_2[0] = getUint(json_document, "Tacho PPR");
	config_2[1] = getUint(json_document, "Fuel Gauge Full");
	config_2[2] = getUint(json_document, "Fuel Gauge Empty");
	config_2[3] = getUint(json_document, "Temp Gauge Hot");
	config_2[4] = getUint(json_document, "Temp Gauge Cold");
	config_2[5] = getDouble(json_document, "BMS Min Voltage") * 100 - 150;
	config_2[6] = getDouble(json_document, "BMS Max Voltage") * 100 - 200;
	config_2[7] = getDouble(json_document, "Balance Voltage") * 100 - 200;

	// Config 3
	uint8_t config_3[8];
	unsigned int temp;

	config_3[0] = getDouble(json_document, "BMS Hysteresis") * 100;
	config_3[1] = getInt(json_document, "BMS Min Temp") + 40;
	config_3[2] = getInt(json_document, "BMS Max Temp") + 40;
	temp = getUint(json_document, "Max Charge Voltage");
	config_3[3] = temp & 0xFF;
	config_3[4] = ((temp >> 1) & 0x80) + (getUint(json_document, "Max Charge Current") & 0x7F);
	temp = getUint(json_document, "Alt Charge Voltage");
	config_3[5] = temp & 0xFF;
	config_3[6] = ((temp >> 1) & 0x80) + (getUint(json_document, "Alt Charge Current") & 0x7F);
	config_3[7] = getUint(json_document, "Sleep Delay");

	// Config 4
	uint8_t config_4[8];

	config_4[0] = getUint(json_document, "MPI Function");
	config_4[1] = getUint(json_document, "MPO1 Function");
	config_4[2] = getUint(json_document, "MPO2 Function");
	config_4[3] = getUint(json_document, "Parallel Strings");
	config_4[4] = getUint(json_document, "Enable Precharge");
	config_4[5] = getUint(json_document, "Stationary Mode");
	config_4[6] = 0;
	config_4[7] = 0;

	// Cell numbers
	uint8_t cell_numbers[8];

	rapidjson::SizeType bms_count = 16;
	std::string bms_member = "BMS Cell Numbers";
	const rapidjson::Value &array = getArray(json_document, bms_member, bms_count);
	for (rapidjson::SizeType i = 0; i < bms_count; i++)
	{
		if (!array[i].IsUint())
		{
			error_at_line(-1, 0, __FILE__, __LINE__, "Member \"%s[%u]\" should be of type %s", bms_member.c_str(), i, "unsigned int");
		}
		unsigned int cell_count = array[i].GetUint();
		if (cell_count > 12)
		{
			error_at_line(-1, 0, __FILE__, __LINE__, "Member \"%s[%u]\" should be no greater than 12", bms_member.c_str(), i);
		}
		if (i % 2)
		{
			cell_numbers[i / 2] += array[i].GetUint() << 4;
		}
		else
		{
			cell_numbers[i / 2] = array[i].GetUint() & 0x0F;
		}
	}

	/* Send config */

	// Write config to CAN
	CAN can(can_device);

	uint8_t start[] = {0x01};
	can.write(0x8000001F, 1, start);

	can.write(0x80000020, 8, config_1);
	can.write(0x80000021, 8, config_2);
	can.write(0x80000022, 8, config_3);
	can.write(0x80000023, 8, config_4);
	can.write(0x80000024, 8, cell_numbers);

	uint8_t end[] = {0x00};
	can.write(0x8000001F, 1, end);
}
