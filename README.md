# Zeva Config
A command line utility for configuring Zeva devices over CAN bus, using a JSON configuration file.


## Usage
```bash
zeva-config CAN_DEVICE CONFIG_FILE
```

`CAN_DEVICE`	is the CAN interface. This is typically `can0`, `can1`, `vcan0`.

`CONFIG_FILE`	is the JSON configuration file. An example configuration file ([`example.json`](example.json)) is provided and the table below describes valid values. Consider consulting the official [CAN bus protocol specification](http://zeva.com.au/Products/datasheets/EVMS3_CAN_Protocol.pdf) for more information.

| Variable            | Units   | Minimum | Maximum | Increment | Special Values
|---------------------|---------|--------:|--------:|----------:|----------------
| Pack Capacity       | Ah      |       5 |    1250 |         5 |
| SoC Warning         | %       |       1 |      99 |         1 | Use 0 to disable.
| Full Voltage        | V       |      10 |     502 |         2 |
| Warn Current        | A       |      10 |    1200 |        10 | Use 1210 to disable.
| Trip Current        | A       |      10 |    1200 |        10 | Use 1210 to disable.
| EVMS Temp Warning   | °C      |       0 |     150 |         1 | Use 151 to disable.
| Min Aux Voltage     | V       |       8 |      15 |         1 |
| Min Isolation       | %       |       0 |      99 |         1 |
| Tacho PPR           |         |       1 |       6 |         1 |
| Fuel Gauge Full     | %       |       0 |     100 |         1 |
| Fuel Gauge Empty    | %       |       0 |     100 |         1 |
| Temp Gauge Hot      | %       |       0 |     100 |         1 |
| Temp Gauge Cold     | %       |       0 |     100 |         1 |
| BMS Min Voltage     | V       |    1.50 |    4.00 |      0.01 |
| BMS Max Voltage     | V       |    2.00 |    4.50 |      0.01 |
| Balance Voltage     | V       |    2.00 |    4.50 |      0.01 | Use 4.51 for dynamic balancing.<br>Use 4.52 to disable balancing.
| BMS Hysteresis      | V       |    0.00 |    0.50 |      0.01 |
| BMS Min Temp        | °C      |     -39 |     101 |         1 | Use -40 to disable.
| BMS Max Temp        | °C      |     -40 |     100 |         1 | Use 101 to disable.
| Max Charge Voltage  | V       |       0 |     511 |         1 |
| Max Charge Current  | A       |       0 |     127 |         1 |
| Alt Charge Voltage  | V       |       0 |     511 |         1 |
| Alt Charge Current  | A       |       0 |     127 |         1 |
| Sleep Delay         | minutes |       1 |       5 |         1 | Use 6 to disable.
| MPI Function        |         |         |         |           | Use 0 for wake up.<br> Use 1 for alt charge.<br> Use 2 for hdlight in.<br> Use 3 for ctr aux sw.
| MPO1 Function       |         |         |         |           | Use 0 for ground.<br> Use 1 for temp gauge.<br> Use 2 for low SoC signal.<br> Use 3 for overtemp signal.<br> Use 4 for undertemp signal.<br> Use 5 for error buzzer.<br> Use 6 for status light.
| MPO2 Function       |         |         |         |         1 | Same as MPO1 Function.
| Parallel Strings    |         |       1 |      20 |         1 |
| Enable Precharge    |         |       0 |       1 |         1 |
| Stationary Mode     |         |       0 |       1 |         1 |
| BMS Cell Numbers    |         |       0 |      12 |         1 |

**Note:** The [manual](http://zeva.com.au/Products/datasheets/EVMS3_Manual.pdf) and the [CAN protocol](http://zeva.com.au/Products/datasheets/EVMS3_CAN_Protocol.pdf) disagree on some of the values specified above. The table chooses to side with the CAN protocol, as this program deals with the CAN bus.


## Supported Devices
Currently this utility supports the configuration of:
- [EVMS3](http://zeva.com.au/index.php?product=131)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.


## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
